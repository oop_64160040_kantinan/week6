package com.kantinan;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {

    @Test
    public void shouldwithdrawSuccess(){
        Bookbank book = new Bookbank("worawit",100);
        book.withdraw(50);
        assertEquals(50, book.getBlance(),0.0001);
    }

    @Test
    public void shouldwithdrawOverBalance(){
        Bookbank book = new Bookbank("worawit",100);
        book.withdraw(150);
        assertEquals(100, book.getBlance(),0.0001);
    }


    @Test
    public void shouldwithdrawwithdrawwithNegativeNumber(){
        Bookbank book = new Bookbank("worawit",100);
        book.withdraw(-100);
        assertEquals(100, book.getBlance(),0.0001);
    }

    @Test
    public void shouldDepositSuccess(){
        Bookbank book = new Bookbank("worawit",100);
        book.deposit(-100);
        assertEquals(100, book.getBlance(),0.0001);
    }


    @Test
    public void shouldMinimumDeposit100Baht(){
        Bookbank book = new Bookbank("worawit",100);
        book.deposit(99);
        assertEquals(100, book.getBlance(),0.0001);
    }


    @Test
    public void shouldDepositMoreThanTheLevel1000000Baht(){
        Bookbank book = new Bookbank("worawit",100);
        book.deposit(1000000000.0);
        assertEquals(100, book.getBlance(),0.0001);
    }
}
