package com.kantinan;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {

    @Test
    public void shouldCreateRobotSuccess1(){
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getsymbol());
        assertEquals(11, robot.getY());
        assertEquals(10, robot.getX());
    }

    @Test
    public void shouldCreateRobotSuccess2(){
        Robot robot = new Robot("Robot",'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getsymbol());
        assertEquals(0, robot.getY());
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldDownOver(){
    Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
    assertEquals(false, robot.down());
    assertEquals(Robot.MAX_Y, robot.getY());
    }


    @Test
    public void shouldUpNegative(){
    Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
    assertEquals(false, robot.up());
    assertEquals(Robot.MIN_Y, robot.getY());
    }


    @Test
    public void shouldDownSuccsee(){
    Robot robot = new Robot("Robot", 'R', 0, 0);
    assertEquals(true, robot.down());
    assertEquals(1, robot.getY());
    }

    @Test
    public void shouldDownNSuccsee(){
    Robot robot = new Robot("Robot", 'R', 10, 11);
    assertEquals(true, robot.down(8));
    assertEquals(3, robot.getY());
    }


    @Test
    public void shouldDownNfail(){
    Robot robot = new Robot("Robot", 'R', 10, 11);
    assertEquals(false, robot.down(12));
    assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpSuccsee(){
    Robot robot = new Robot("Robot", 'R', 0, 1);
    assertEquals(true, robot.up());
    assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNSuccsee1(){
    Robot robot = new Robot("Robot", 'R', 10, 11);
    assertEquals(true, robot.up(5));
    assertEquals(6, robot.getY());
    }

    @Test
    public void shouldUpNSuccsee2(){
    Robot robot = new Robot("Robot", 'R', 10, 11);
    assertEquals(true, robot.up(11));
    assertEquals(0, robot.getY());
    }


    @Test
    public void shouldUpNFail(){
    Robot robot = new Robot("Robot", 'R', 10, 11);
    assertEquals(false, robot.up(12));
    assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRightOver(){
    Robot robot = new Robot("Robot", 'R', Robot.MAX_X, 0);
    assertEquals(false, robot.right());
    assertEquals(Robot.MAX_X, robot.getX());
    }

    @Test
    public void shouldLeftNegative(){
    Robot robot = new Robot("Robot", 'R', Robot.MIN_X, 0);
    assertEquals(false, robot.left());
    assertEquals(Robot.MIN_X, robot.getX());
    }


    @Test
    public void shouldRightSuccsee(){
    Robot robot = new Robot("Robot", 'R', 0, 0);
    assertEquals(true, robot.right());
    assertEquals(robot.getX(), 1);
    }
    @Test
    public void shouldRightNSuccsee(){
    Robot robot = new Robot("Robot", 'R', 10, 10);
    assertEquals(true, robot.right(7));
    assertEquals(robot.getX(), 10);
    }

    
    @Test
    public void shouldRightNFail(){
    Robot robot = new Robot("Robot", 'R', 10, 10);
    assertEquals(false, robot.right(13));
    assertEquals(robot.getX(), 10);
    }
    @Test
    public void shouldLeftSuccsee(){
    Robot robot = new Robot("Robot", 'R', 1, 0);
    assertEquals(true, robot.left());
    assertEquals(robot.getX(), 0);
    }

    @Test
    public void shouldLeftNSuccsee(){
    Robot robot = new Robot("Robot", 'R', 11, 10);
    assertEquals(true, robot.left(9));
    assertEquals(robot.getX(), 11);
    }


    @Test
    public void shouldLeftNFail(){
    Robot robot = new Robot("Robot", 'R', 11, 10);
    assertEquals(false, robot.left(14));
    assertEquals(robot.getX(), 11);
    }
}
