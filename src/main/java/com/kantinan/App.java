package com.kantinan;

public class App 
{
    public static void main( String[] args )
    {
        Bookbank worawit = new Bookbank("worawit",50.0);
        worawit.print();
        Bookbank prayud = new Bookbank("PraYuddd",100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        worawit.deposit(400000.0);
        worawit.print();

        Bookbank prawit = new Bookbank("Prawit",100000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();

    }

}
