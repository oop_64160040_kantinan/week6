package com.kantinan;

public class Bookbank {
    //Attributes
    private String name;
    private double blance;
    //public BookBank()
    public Bookbank(String name,double balance){ //construtor
        this.name = name;
        this.blance = balance;
    }
    //Methods
    public boolean deposit(double money){
        if(money<1)return false;
        if(money<100)return false;
        if(money>10000000)return false;
        blance = blance + money;
        return true;
    }
    public boolean withdraw(double money){
        if(money<1)return false;
        if(money>blance)return false;
        blance = blance - money;
        return true;
    }
    public void print(){
        System.out.println(name + " " + blance);
    }

    public String getName() {
        return name;
    }

    public double getBlance() {
        return blance;
    }
}
